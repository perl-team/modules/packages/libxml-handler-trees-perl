libxml-handler-trees-perl (0.02-9) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libxml-handler-trees-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 19:02:20 +0100

libxml-handler-trees-perl (0.02-8) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use versioned copyright format URI.
  * Update copyright file header to use current field names (Name =>
    Upstream-Name, Maintainer => Upstream-Contact)
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Replace spaces in short license names with dashes.
  * Refer to specific version of license GPL-1+.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 28 Jun 2022 23:03:36 +0100

libxml-handler-trees-perl (0.02-7) unstable; urgency=medium

  * Team upload.

  [ Ansgar Burchardt ]
  * Update my email address.
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Remove Jay Bonci from Uploaders. Thanks for your work!

  [ Niko Tyni ]
  * Update to debhelper compat level 10
  * Update to Standards-Version 4.1.3
  * Update to dpkg source format 3.0 (quilt)
  * Make the package autopkgtestable

 -- Niko Tyni <ntyni@debian.org>  Thu, 18 Jan 2018 22:19:20 +0200

libxml-handler-trees-perl (0.02-6) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza).
  * Set Maintainer to Debian Perl Group.
  * Use dist-based URL in debian/watch.
  * debian/control: Added: ${misc:Depends} to Depends: field.

  [ Ansgar Burchardt ]
  * Refresh rules for debhelper 7.
  * Convert debian/copyright to proposed machine-readable format.
  * debian/watch: Use extended regular expression to match upstream releases.
  * debian/control: Remove duplicate Section, Priority fields from binary
    package stanza.
  * debian/control: Move debhelper to Build-Depends (instead of B-D-Indep).
  * debian/control: Mention module name in description.
  * Bump Standards-Version to 3.8.3.
  * Add myself to Uploaders.

 -- Ansgar Burchardt <ansgar@43-1.org>  Sun, 17 Jan 2010 13:34:14 +0900

libxml-handler-trees-perl (0.02-5) unstable; urgency=low

  * Adds debian/watch file so uscan will work

 -- Jay Bonci <jaybonci@debian.org>  Wed, 27 Oct 2004 11:34:50 -0400

libxml-handler-trees-perl (0.02-4) unstable; urgency=low

  * New Maintainer (Closes: #210531)
  * Bumped policy-version to 3.6.1.0 (No other changes)
  * Removed .install file as it's no longer needed
  * Clarified copyright explicating dual nature of perl license
  * Moved to my standard rules template

 -- Jay Bonci <jaybonci@debian.org>  Thu,  4 Mar 2004 01:05:30 -0500

libxml-handler-trees-perl (0.02-3) unstable; urgency=low

  * debian/rules: moved debhelper compatibility level setting to
    'debian/compat' per latest debhelper best practices
  * debian/control: updated sections according to latest archive changes:
    - 'libxml-handler-trees-perl' from 'interpreters' to 'perl'
  * debian/control: upgraded build dependency on 'debhelper' to '>= 4.1'
  * debian/control: upgraded to Debian Policy 3.6.0 (no changes)

 -- Ardo van Rangelrooij <ardo@debian.org>  Mon,  1 Sep 2003 11:30:12 -0500

libxml-handler-trees-perl (0.02-2) unstable; urgency=low

  * debian/rules: upgraded to debhelper v4
  * debian/control: changed build dependency on debhelper accordingly
  * debian/rules: migrated from 'dh_movefiles' to 'dh_install'
  * debian/rules: split off 'install' target from 'binary-indep' target
  * debian/copyright: added pointer to license

 -- Ardo van Rangelrooij <ardo@debian.org>  Tue,  6 Aug 2002 20:40:20 -0500

libxml-handler-trees-perl (0.02-1) unstable; urgency=low

  * Initial release

 -- Ardo van Rangelrooij <ardo@debian.org>  Mon, 31 Dec 2001 19:29:19 -0600
